package com.example.examenrecuperacion;

import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

public class MainActivity extends AppCompatActivity {

    private EditText txtCliente;
    private Button btnCotizacion;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        // Obtener referencias a los componentes de la interfaz de usuario
        txtCliente = findViewById(R.id.txtCliente);
        btnCotizacion = findViewById(R.id.btnCotizacion);

        // Asignar un Listener al botón de Cotización
        btnCotizacion.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // Obtener el nombre del cliente ingresado
                String cliente = txtCliente.getText().toString();


                Intent intent = new Intent(MainActivity.this, CotizacionActivity.class);
                intent.putExtra("cliente", cliente); // Pasar el nombre del cliente como parámetro


                startActivity(intent);
            }
        });
    }
}
