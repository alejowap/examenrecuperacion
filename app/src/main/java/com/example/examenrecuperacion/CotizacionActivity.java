package com.example.examenrecuperacion;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;

public class CotizacionActivity extends AppCompatActivity {
    private TextView lblNombre;
    private TextView lblFolio;
    private EditText txtDescripcion;
    private EditText txtValorAuto;
    private EditText txtPorEng;
    private RadioButton rdb12;
    private RadioButton rdb18;
    private RadioButton rdb36;
    private EditText txtMensual;
    private Button btnCalcular;
    private Button btnLimpiar;
    private Button btnRegresar;
    private TextView lblPagoMensual;
    private TextView lblEnfanche;
    private Cotizacion cotizacion;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.cotizacion_layout);

        // Inicializar los componentes de la interfaz
        lblNombre = findViewById(R.id.lblNombre);
        lblFolio = findViewById(R.id.lblFolio);
        txtDescripcion = findViewById(R.id.txtDescripcion);
        txtValorAuto = findViewById(R.id.txtValorAuto);
        txtPorEng = findViewById(R.id.txtPorEng);
        rdb12 = findViewById(R.id.rdb12);
        rdb18 = findViewById(R.id.rdb18);
        rdb36 = findViewById(R.id.rdb36);
        txtMensual = findViewById(R.id.txtMensual);
        btnCalcular = findViewById(R.id.btnCalcular);
        btnLimpiar = findViewById(R.id.btnLimpiar);
        btnRegresar = findViewById(R.id.btnRegresar);
        lblPagoMensual = findViewById(R.id.lblPagoMensual);
        lblEnfanche = findViewById(R.id.lblEnfanche);
        cotizacion = new Cotizacion();

        btnCalcular.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // Obtener los valores ingresados por el usuario
                String descripcion = txtDescripcion.getText().toString();
                float valorAuto = Float.parseFloat(txtValorAuto.getText().toString());
                float porEnganche = Float.parseFloat(txtPorEng.getText().toString());
                int plazo = 0;
                if (rdb12.isChecked()) {
                    plazo = 12;
                } else if (rdb18.isChecked()) {
                    plazo = 18;
                } else if (rdb36.isChecked()) {
                    plazo = 36;
                }

                // Configurar los valores en la cotización
                cotizacion.setDescripcion(descripcion);
                cotizacion.setValorAuto(valorAuto);
                cotizacion.setPorEnganche(porEnganche);
                cotizacion.setPlazo(plazo);

                // Calcular el enganche y el pago mensual
                float enganche = cotizacion.calcularEnganche();
                float pagoMensual = cotizacion.calcularPagoMensual();

                // Mostrar los resultados en los TextView correspondientes
                lblEnfanche.setText(String.format("%.2f", enganche));
                lblPagoMensual.setText(String.format("%.2f", pagoMensual));
            }
        });

        btnLimpiar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // Limpiar los campos de texto y los resultados
                txtDescripcion.setText("");
                txtValorAuto.setText("");
                txtPorEng.setText("");
                rdb12.setChecked(false);
                rdb18.setChecked(false);
                rdb36.setChecked(false);
                txtMensual.setText("");
                lblEnfanche.setText("");
                lblPagoMensual.setText("");
            }
        });

        btnRegresar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // Finalizar la actividad y regresar a la actividad anterior
                finish();
            }
        });
    }
}
