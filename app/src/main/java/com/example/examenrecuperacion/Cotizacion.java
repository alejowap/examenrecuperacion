package com.example.examenrecuperacion;

public class Cotizacion {
    private static int ultimoFolio = 0; // Variable estática para almacenar el último folio generado

    private int folio;
    private String descripcion;
    private float valorAuto;
    private float porEnganche;
    private int plazo;

    public Cotizacion() {
        this.folio = generarFolio(); // Generar automáticamente el folio al crear una nueva instancia de Cotizacion
    }

    private static int generarFolio() {
        ultimoFolio++; // Incrementar el último folio generado
        return ultimoFolio;
    }

    public float calcularEnganche() {
        float enganche = valorAuto * (porEnganche / 100);
        return enganche;
    }

    public float calcularPagoMensual() {
        float enganche = calcularEnganche();
        float montoFinanciar = valorAuto - enganche;
        float pagoMensual = montoFinanciar / plazo;
        return pagoMensual;
    }

    // Getters y Setters

    public int getFolio() {
        return folio;
    }

    public void setFolio(int folio) {
        this.folio = folio;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public float getValorAuto() {
        return valorAuto;
    }

    public void setValorAuto(float valorAuto) {
        this.valorAuto = valorAuto;
    }

    public float getPorEnganche() {
        return porEnganche;
    }

    public void setPorEnganche(float porEnganche) {
        this.porEnganche = porEnganche;
    }

    public int getPlazo() {
        return plazo;
    }

    public void setPlazo(int plazo) {
        this.plazo = plazo;
    }
}

